const mongoose = require('mongoose');
const timestamps = require('mongoose-timestamp');


const StoresSchema = new mongoose.Schema(
    {
        "state": {
            "type": "String"
        },
        "nomeLoja": {
            "type": "String"
        },
        "movies": [
            {
                "movie": {
                    "type": "String"
                },
                "quantity": {
                    "type": "number"
                }
            }
        ]

    }

);

StoresSchema.plugin(timestamps);

const StoresModel= mongoose.model('stores', StoresSchema, 'stores');
module.exports = StoresModel;