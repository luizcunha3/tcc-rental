const mongoose = require('mongoose');
const axios = require('axios');
const Stores = require('../models/stores');
const config = require('../config');

const handleConnection = _ => {
    return new Promise(((resolve, reject) => {
        mongoose.Promise = global.Promise;
        mongoose.connect(config.db.uri, {useNewUrlParser: true});
        const db = mongoose.connection;

        db.on('error', (err) => {
            reject('Could not connect to mongo')
        });

        db.once('open', () => {
            resolve(true)
        });
    }))

};

const getStores = (req, res, next) => {
    if(req.params.id){
        let storesGlobal = {}
        return Stores.findById(req.params.id)
            .then(stores => {
                res.send(200, stores);
                return true;
            })
            .catch(err => {
                console.error(err);
                return false
            })
    } else {
        const query = {};

        return Stores.find(query)
            .then(stores => {
                res.send(200, stores);
                return true
            })
            .catch(err => {
                console.error(err);
                return false;
            })
    }
};

module.exports = function(server) {
    server.get('/', (req, res, next) => {
        res.send(200, "Rental is running")
    });
    server.get('/stores', (req, res, next) => {
       handleConnection()
           .then(status => {
               return getStores(req, res, next)
           })
           .then(res => {
               mongoose.disconnect();
           })
           .catch(err => {
               mongoose.disconnect();
               res.send(500)
           })
    })
};