module.exports = {
    name: 'Rental',
    env: process.env.NODE_ENV || 'development',
    port: process.env.PORT || 3002,
    base_url: process.env.BASE_URL || 'http://localhost:3002',
    db: {
        uri: process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/tcc',
    },
    services: {
        movieDatabase: process.env.MOVIE_DATABASE_URI || 'http://localhost:3000'
    }
};